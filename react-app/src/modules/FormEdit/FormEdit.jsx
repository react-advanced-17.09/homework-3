import React from "react";
import Button from "../../components/Button/Button";
import "./FormEdit.scss";

class FormEdit extends React.PureComponent {
  state = {
    inputValue: "",
    textareaValue: "",
  };

  onChangedHandler = ({ value, name }) => {
    this.setState({
      [name]: value,
    });
  };

  onSave = (event) => {
    event.preventDefault();

    const { onSaveEditHandler, hideModalHandler, post } = this.props;
    const { inputValue, textareaValue } = this.state;

    onSaveEditHandler(post.id, inputValue, textareaValue);

    hideModalHandler();
  };

  componentDidMount() {
    const { post } = this.props;
    
    this.setState({
      inputValue: post.title,
      textareaValue: post.body,
    });
  }

  render() {
    const { inputValue, textareaValue } = this.state;
    const { hideModalHandler } = this.props;

    return (
      <form className="form" onSubmit={this.onSave}>
        <div className="form-controls">
          <div className="form-group">
            <label className="form-label" htmlFor="title" />
            <input
              className="form-control"
              id="title"
              type="text"
              placeholder="Title"
              name="inputValue"
              value={inputValue}
              onChange={(event) => {
                this.onChangedHandler(event.target);
              }}
            />
          </div>
          <div className="form-group">
            <label className="form-label" htmlFor="desrc" />
            <textarea
              className="form-textarea"
              id="desrc"
              placeholder="Description"
              name="textareaValue"
              value={textareaValue}
              onChange={(event) => {
                this.onChangedHandler(event.target);
              }}
            />
          </div>
        </div>
        <div className="btn-group">
          <Button buttonType={"submit"} buttonText={"Save"} />
          <Button
            buttonType={"button"}
            buttonText={"Close"}
            handleChange={hideModalHandler}
          />
        </div>
      </form>
    );
  }
}

export default FormEdit;
