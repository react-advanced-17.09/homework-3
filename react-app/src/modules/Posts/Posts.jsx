import React from "react";
import PostCard from "../../components/PostCard/PostCard";
import Modal from "../../components/Modal/Modal";
import FormEdit from "../../modules/FormEdit/FormEdit";
import { ToastContainer, toast } from "react-toastify";
import { requestData, requestDeletePost, requestEditPost } from "../../api";
import "react-toastify/dist/ReactToastify.css";
import "./Posts.scss";

class Posts extends React.Component {
  state = {
    posts: [],
    isShowModal: false,
    editedPostId: null,
  };

  showModalHandler = (id) => {
    this.setState({
      isShowModal: true,
      editedPostId: id,
    });
  };

  notify = () =>
    toast.success("Post successfully deleted", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

  deletePostHandler = async (id) => {
    const { posts } = this.state;
    this.setState({
      posts: posts.filter((item) => item.id !== id),
    });

    this.notify();

    await requestDeletePost(id);

    // api request without deleted posts
    // await this.setPoststListState();
  };

  onSaveEditHandler = async (id, editedTitle, editedBody) => {
    await requestEditPost(id, editedTitle, editedBody);

    const editedPost = this.state.posts.find((item) => item.id === id);

    editedPost.title = editedTitle;
    editedPost.body = editedBody;

    this.setState({
      posts: this.state.posts,
    });

    // api request with edited posts
    // await this.setPoststListState();
  };

  hideModalHandler = () => {
    this.setState({ isShowModal: false });
  };

  setPoststListState = async () => {
    const postsList = await requestData();
    this.setState({
      posts: postsList,
    });
  };

  componentDidMount() {
    this.setPoststListState();
  }

  render() {
    const { posts } = this.state;

    const currentPost = posts.find(
      (item) => item.id === this.state.editedPostId
    );

    return (
      <div className="posts">
        {this.state.posts.map((post) => (
          <PostCard
            key={post.id}
            title={post.title}
            body={post.body}
            postId={post.id}
            showModalHandler={this.showModalHandler}
            deletePostHandler={this.deletePostHandler}
          />
        ))}
        {this.state.isShowModal && (
          <Modal
            show={this.state.isShowModal}
            hideModalHandler={this.hideModalHandler}
          >
            <FormEdit
              post={currentPost}
              onSaveEditHandler={this.onSaveEditHandler}
              hideModalHandler={this.hideModalHandler}
            />
          </Modal>
        )}
        <ToastContainer />
      </div>
    );
  }
}

export default Posts;
