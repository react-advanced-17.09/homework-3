import React from "react";
import "./Button.scss";

class Button extends React.Component {
  render() {
    const { buttonText, handleChange, buttonType } = this.props;

    return (
      <button className="btn" type={buttonType} onClick={handleChange}>
        {buttonText}
      </button>
    );
  }
}

export default Button;
