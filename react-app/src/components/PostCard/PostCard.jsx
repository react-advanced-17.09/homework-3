import React from "react";
import Button from "../Button/Button";
import "./PostCard.scss";

class PostCard extends React.Component {
  render() {
    const { title, body, showModalHandler, deletePostHandler, postId } =
      this.props;
    return (
      <div className="post">
        <span className="post-title">{title}</span>
        <p className="post-body">{body}</p>
        <div className="post-action">
          <Button
            buttonText={"Edit"}
            buttonType={"button"}
            handleChange={() => showModalHandler(postId)}
          />
          <Button
            buttonText={"Delete"}
            buttonType={"submit"}
            handleChange={() => deletePostHandler(postId)}
          />
        </div>
      </div>
    );
  }
}

export default PostCard;
