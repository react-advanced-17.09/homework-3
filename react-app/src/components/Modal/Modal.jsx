import React from "react";
import "./Modal.scss";

class Modal extends React.Component {
  render() {
    const { show, children } = this.props;
    return (
      <div className={show ? "modal-ovarlay is-show" : "modal-ovarlay"}>
        <div className="modal">
          <div className="modal-header">Edit Post Card</div>
          <div className="modal-body">
            {children}
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
