const BASE_URL = 'https://jsonplaceholder.typicode.com/';

export function requestData() {
    return fetch(`${BASE_URL}posts`).then((response) => response.json());
}

export async function requestDeletePost(id) {
    return await fetch(`${BASE_URL}posts/${id}`, {
        method: 'DELETE',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
}

export async function requestEditPost(id, editedTitle, editedBody) {
    console.log(id, 'api id')
    return await fetch(`${BASE_URL}posts/${id}`, {
        method: 'PATCH',
        body: JSON.stringify({
            title: editedTitle,
            body: editedBody,
        }),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    }).then((response) => response.json());
}
