import './App.scss';
import Posts from './modules/Posts/Posts';

function App() {
  return (
    <div className="App">
      <Posts/>
    </div>
  );
}

export default App;
